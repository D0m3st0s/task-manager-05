package ru.shumov.tm.command;

public abstract class AbstractCommand {
    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public abstract void execute();
}
