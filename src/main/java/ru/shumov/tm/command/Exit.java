package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;

public class Exit extends AbstractCommand{
    private String name = "exit";
    private String description = "exit: Остановка программы.";
    private Bootstrap bootstrap;
    private Boolean work = true;

    public Exit(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() {
        bootstrap.setWork(work == false);
    }
}
