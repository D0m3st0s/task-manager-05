package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.service.Data;

import java.util.Collection;

public class TaskGetListCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "task list";
    private String description = "task list: Вывод всех задач.";

    public TaskGetListCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        Collection<Task> values = bootstrap.getTaskService().getList();
        if(values.isEmpty()){
            data.outPutString(Commands.TASKS_DO_NOT_EXIST);
        } else {
            for(Task task : values){
                data.outPutTask(task);
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
