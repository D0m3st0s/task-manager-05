package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.service.Data;


import java.util.Collection;

public class ProjectRemoveCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "project remove";
    private String description = "project remove: Выборочное удаление проектов.";

    public ProjectRemoveCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        data.outPutString(Commands.ENTER_PROJECT_ID_FOR_REMOVING);
        String projectId = data.scanner();
        bootstrap.getProjectService().remove(projectId);
        Collection<Task> values = bootstrap.getTaskService().getList();
        for(Task task : values) {
            if(task.getProjectId().equals(projectId)){
                values.remove(task);
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
