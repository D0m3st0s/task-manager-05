package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.service.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TaskUpdateCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "task update";
    private String description = "task update: Изменения параметров задачи.";

    public TaskUpdateCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        data.outPutString(Commands.ENTER_ID_OF_TASK_FOR_SHOWING);
        String taskId = data.scanner();
        try {
            if (bootstrap.getTaskService().checkKey(taskId)) {
                Task task = bootstrap.getTaskService().getOne(taskId);
                data.outPutString(Commands.ENTER_TASK_NAME);
                String name = data.scanner();
                data.outPutString(Commands.ENTER_DESCRIPTION_OF_TASK);
                String description = data.scanner();
                data.outPutString(Commands.ENTER_START_DATE_OF_TASK);
                String startDate = data.scanner();
                data.outPutString(Commands.ENTER_DEADLINE_OF_PROJECT);
                String endDate = data.scanner();
                task.setName(name);
                task.setDescription(description);
                task.setStartDate(format.parse(startDate));
                task.setEndDate(format.parse(endDate));
                bootstrap.getTaskService().update(task);
            }
        }
        catch (ParseException parseException) {
            parseException.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
