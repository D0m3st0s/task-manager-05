package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;

public class Help extends AbstractCommand{
    private String name = "help";
    private String description = "help : Вывод доступных команд.";
    private Bootstrap bootstrap;

    public Help(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }


    @Override
    public void execute() {
        bootstrap.help();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
