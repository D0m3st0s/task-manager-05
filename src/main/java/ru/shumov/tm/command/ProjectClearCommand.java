package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.service.Data;

public class ProjectClearCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "project clear";
    private String description = "project clear: Удаление всех проектов.";

    public ProjectClearCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        data.outPutString(Commands.ALL_PROJECTS_WILL_BE_CLEARED);
        data.outPutString(Commands.YES_NO);
        String answer = data.scanner();
        if(answer.equals(Commands.YES)) {
            bootstrap.getProjectService().clear();
            bootstrap.getTaskService().clear();
        }

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
