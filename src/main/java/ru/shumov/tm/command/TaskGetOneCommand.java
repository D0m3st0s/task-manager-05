package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.service.Data;

public class TaskGetOneCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "get task";
    private String description = "get task: вывод конкретной задачи.";

    public TaskGetOneCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        data.outPutString(Commands.ENTER_TASK_ID_FOR_SHOWING_TASKS);
        String taskId = data.scanner();
        try {
            if (taskId.isEmpty() || taskId == null) {
                throw new IllegalArgumentException();
            }
            data.outPutTask(bootstrap.getTaskService().getOne(taskId));
        }
        catch (IllegalArgumentException illegalArgumentException) {
            illegalArgumentException.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
