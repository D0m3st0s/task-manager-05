package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.service.Data;


import java.util.Collection;

public class ProjectGetListCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "project list";
    private String description = "project list: Вывод всех проектов.";

    public ProjectGetListCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        Collection<Project> values = bootstrap.getProjectService().getList();
        if(values.isEmpty()){
            data.outPutString(Commands.PROJECTS_DO_NOT_EXIST);
        } else {
            for (Project project : values) {
                data.outPutProject(project);
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
