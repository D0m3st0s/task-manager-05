package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.service.Data;


import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ProjectUpdateCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "project update";
    private String description = "project update: Изменение параметров проекта.";

    public ProjectUpdateCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        data.outPutString(Commands.ENTER_ID_OF_PROJECT_FOR_SHOWING);
        String projectId = data.scanner();
        try {
            if (bootstrap.getProjectService().checkKey(projectId)) {
                Project project = bootstrap.getProjectService().getOne(projectId);
                System.out.println();
                String name = data.scanner();
                System.out.println();
                String description = data.scanner();
                System.out.println();
                String startDate = data.scanner();
                System.out.println();
                String endDate = data.scanner();
                project.setName(name);
                project.setDescription(description);
                project.setStartDate(format.parse(startDate));
                project.setEndDate(format.parse(endDate));
                bootstrap.getProjectService().update(project);
            }
        }
        catch (ParseException parseException) {
            parseException.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
