package ru.shumov.tm.command;


import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.service.Data;

public class TaskClearCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "task clear";
    private String description = "task clear: Удаление всех задач.";

    public TaskClearCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        data.outPutString(Commands.ALL_TASKS_WILL_BE_CLEARED);
        data.outPutString(Commands.YES_NO);
        String answer = data.scanner();
        if(Commands.YES.equals(answer)) {
            bootstrap.getTaskService().clear();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
