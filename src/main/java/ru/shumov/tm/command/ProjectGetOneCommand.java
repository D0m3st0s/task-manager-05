package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.service.Data;


public class ProjectGetOneCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "get project";
    private String description = "get project: Вывод конкретного проекта.";

    public ProjectGetOneCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        data.outPutString(Commands.ENTER_PROJECT_ID);
        String projectId = data.scanner();
        Project project = bootstrap.getProjectService().getOne(projectId);
        data.outPutProject(project);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
