package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.service.Data;

public class TaskRemoveCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private String name = "task remove";
    private String description = "task remove: Выборочное удаление задач.";

    public TaskRemoveCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        data.outPutString(Commands.ENTER_TASK_ID_FOR_REMOVING);
        String taskId = data.scanner();
        try {
            if (taskId == null || taskId.isEmpty()) {
                throw new NullPointerException();
            }
            bootstrap.getTaskService().remove(taskId);
        }
        catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
