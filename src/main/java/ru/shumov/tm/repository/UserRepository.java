package ru.shumov.tm.repository;

import ru.shumov.tm.entity.User;

import java.util.Map;

public class UserRepository {

    private Map<String, User> Users;

    public UserRepository(Map<String, User> users) {
        Users = users;
    }

    public void authorization() {

    }

    public void logOut() {

    }

    public void registration() {

    }

    public void passwordUpdate() {

    }

    public void show() {

    }

    public void profileEditing() {

    }
}
