package ru.shumov.tm.service;

import ru.shumov.tm.entity.Task;
import ru.shumov.tm.repository.TaskRepository;

import java.text.ParseException;
import java.util.Collection;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository bootstrapTaskRepository) {
        this.taskRepository = bootstrapTaskRepository;
    }

    public void create(Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new NullPointerException();
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new NullPointerException();
            }
            taskRepository.persist(task);
        }
        catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();
        }
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public void remove(String taskId) {
        taskRepository.remove(taskId);
    }

    public Collection<Task> getList() {
        Collection<Task> value = taskRepository.findAll();
        return value;
    }

    public Task getOne(String id) {
        return taskRepository.findOne(id);
    }

    public void update(Task task) throws ParseException {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new NullPointerException();
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new NullPointerException();
            }
            taskRepository.merge(task);
        }
        catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();
        }
    }

    public boolean checkKey(String id) {
        return taskRepository.findOne(id).getId().equals(id);
    }
}
