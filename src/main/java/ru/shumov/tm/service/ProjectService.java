package ru.shumov.tm.service;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.repository.ProjectRepository;
import java.util.Collection;

public class ProjectService {
    private ProjectRepository projectRepository;
    private TaskService taskService;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void create(Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new NullPointerException();
            }
            projectRepository.persist(project.getId(), project);
        }
        catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();
        }
    }

    public void clear() {
        projectRepository.removeAll();
        taskService.clear();
    }

    public void remove(String projectId) {
        try {
            if (projectId == null || projectId.isEmpty()) {
                throw new NullPointerException();
            }
            projectRepository.remove(projectId);
        }
        catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();
        }
    }

    public Collection<Project> getList() {
        Collection<Project> values = projectRepository.findAll();
        return values;
    }

    public Project getOne(String id) {
        return projectRepository.findOne(id);
    }

    public void update(Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new NullPointerException();
            }
            projectRepository.merge(project);
        }
        catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();
        }
    }

    public Boolean checkKey(String id) {
        return projectRepository.findOne(id).getId().equals(id);
    }
}
