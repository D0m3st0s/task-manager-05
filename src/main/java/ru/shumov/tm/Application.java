package ru.shumov.tm;

public class Application {
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.commandsInit();
        bootstrap.start();
    }
}
