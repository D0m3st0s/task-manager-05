package ru.shumov.tm;

import ru.shumov.tm.command.*;
import ru.shumov.tm.repository.ProjectRepository;
import ru.shumov.tm.repository.TaskRepository;
import ru.shumov.tm.service.Data;
import ru.shumov.tm.service.ProjectService;
import ru.shumov.tm.service.TaskService;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static ru.shumov.tm.Commands.*;

public class Bootstrap {
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private Data data;
    private Map<String, AbstractCommand> commands = new HashMap<>();
    private boolean work = true;

    public void init() {
        data = new Data();
        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    public void start() {
        data.outPutString(WELCOME);
        data.outPutString(PROJECT_ID);
        while (work == true) {
            try {
                String commandName = data.scanner();
                if(commands.containsKey(commandName)) {
                    commands.get(commandName).execute();
                } else {
                    data.outPutString(COMMAND_DOES_NOT_EXIST);
                }
            }
            catch (NullPointerException nullPointerException) {
                nullPointerException.printStackTrace();
            }
        }
    }

    public void commandsInit() {
        ProjectCreateCommand projectCreateCommand = new ProjectCreateCommand(data, this);
        commands.put(projectCreateCommand.getName(), projectCreateCommand);
        ProjectClearCommand projectClearCommand = new ProjectClearCommand(data, this);
        commands.put(projectClearCommand.getName(), projectClearCommand);
        ProjectRemoveCommand projectRemoveCommand = new ProjectRemoveCommand(data, this);
        commands.put(projectRemoveCommand.getName(), projectRemoveCommand);
        ProjectGetListCommand projectGetListCommand = new ProjectGetListCommand(data, this);
        commands.put(projectGetListCommand.getName(), projectGetListCommand);
        ProjectGetOneCommand projectGetOneCommand = new ProjectGetOneCommand(data, this);
        commands.put(projectGetOneCommand.getName(), projectGetOneCommand);
        ProjectUpdateCommand projectUpdateCommand = new ProjectUpdateCommand(data, this);
        commands.put(projectUpdateCommand.getName(), projectUpdateCommand);
        TaskCreateCommand taskCreateCommand = new TaskCreateCommand(data, this);
        commands.put(taskCreateCommand.getName(), taskCreateCommand);
        TaskClearCommand taskClearCommand = new TaskClearCommand(data, this);
        commands.put(taskClearCommand.getName(), taskClearCommand);
        TaskRemoveCommand taskRemoveCommand = new TaskRemoveCommand(data, this);
        commands.put(taskRemoveCommand.getName(), taskRemoveCommand);
        TaskGetListCommand taskGetListCommand = new TaskGetListCommand(data, this);
        commands.put(taskGetListCommand.getName(), taskGetListCommand);
        TaskGetOneCommand taskGetOneCommand = new TaskGetOneCommand(data, this);
        commands.put(taskGetOneCommand.getName(), taskGetOneCommand);
        TaskUpdateCommand taskUpdateCommand = new TaskUpdateCommand(data, this);
        commands.put(taskUpdateCommand.getName(), taskUpdateCommand);
        Help help = new Help(this);
        commands.put(help.getName(), help);
        Exit exit = new Exit(this);
        commands.put(exit.getName(), exit);
    }

    public void help() {
        Collection<AbstractCommand> value = commands.values();
        for(AbstractCommand abstractCommand : value){
            data.outPutString(abstractCommand.getDescription());
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }
}
